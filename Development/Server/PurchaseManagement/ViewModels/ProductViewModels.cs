﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurchaseManagement.Models;

namespace PurchaseManagement.ViewModels
{
    public class SearchProductViewModel
    {
        public string Ref { get; set; }

        public string So { get; set; }

        public string Supplier { get; set; }

        public string Status { get; set; }

        public string Team { get; set; }

        public string Keyword { get; set; }

    }
}