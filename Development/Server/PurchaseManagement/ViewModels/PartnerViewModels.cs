﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurchaseManagement.Models;

namespace PurchaseManagement.ViewModels
{
    public class SearchPartnerViewModel
    {
        public string Name { get; set; }
    }

    public class CreatePartnerViewModel : Partner
    {
    }
}