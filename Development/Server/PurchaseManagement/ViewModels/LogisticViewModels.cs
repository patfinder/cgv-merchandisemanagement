﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurchaseManagement.Models;

namespace PurchaseManagement.ViewModels
{
    public class SearchLogisticViewModel
    {
    }

    public class AssignLinesLogisticViewModel
    {
        public Logistic Logistic { get; set; }

        public IList<LogisticLine> AvailableLines { get; set; }

        public IList<LogisticLine> Lines { get; set; }
    }
}