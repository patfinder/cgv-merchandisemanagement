﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using PurchaseManagement.Models;

namespace PurchaseManagement.ViewModels
{
    public class SearchSupplierViewModels
    {
        public string Name { get; set; }
    }

    public class CreateSupplierViewModels : Supplier
    {
    }

    public class CompanyContactsViewModels
    {
        public Supplier Company { get; set; }

        public IEnumerable<Contact> Contacts { get; set; }
    }

    public class CompanyFactoriesViewModels
    {
        public Supplier Company { get; set; }

        public IEnumerable<Factory> Factories { get; set; }
    }

    public class CompanyAddressesViewModels
    {
        public Company Company { get; set; }

        public IEnumerable<Address> Addresses { get; set; }
    }
}