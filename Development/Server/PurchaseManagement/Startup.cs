﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PurchaseManagement.Startup))]
namespace PurchaseManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
