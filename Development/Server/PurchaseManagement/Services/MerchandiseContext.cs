﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using PurchaseManagement.Models;

namespace PurchaseManagement.Services
{
    public class MerchandiseContext : DbContext
    {
        public MerchandiseContext() : base("DefaultConnection")
        {
        }

        public DbSet<Supplier> Suppliers { get; set; }

        public DbSet<Partner> Partners { get; set; }

        public DbSet<CompanyDetails> CompanyDetails { get; set; }

        public DbSet<Contact> Contacts { get; set; }

        public DbSet<Factory> Factories { get; set; }

        public DbSet<Address> Addresses { get; set; }

        public DbSet<Product> Products { get; set; }

        public DbSet<Logistic> Logistics { get; set; }

        public DbSet<LogisticLine> LogisticLines { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}