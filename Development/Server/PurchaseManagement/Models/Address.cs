﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Address
    {
        public int Id { get; set; }

        public CompanyType CompanyType { get; set; }

        public int CompanyId { get; set; }

        public string CreatedDate { get; set; }

        public string Type { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        [Display(Name = "Address")]
        public string AddressString { get; set; }
    }

    public enum CompanyType
    {
        Supplier = 1,
        Partner = 2
    }
}