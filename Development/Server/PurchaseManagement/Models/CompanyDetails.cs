﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class CompanyDetails
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }
        
        /*------------------- Primary Info -------------------*/

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        [Display(Name = "Number Of Employees")]
        public string NoOfEmployees { get; set; }

        [Display(Name = "Turn Over N1")]
        public string TurnOverN1 { get; set; }

        [Display(Name = "Turn Over N2")]
        public string TurnOverN2 { get; set; }

        [Display(Name = "Export Percentage")]
        public string ExportPercentage { get; set; }

        [Display(Name = "Closest Airport")]
        public string ClosestAirport { get; set; }

        [Display(Name = "Timing by car From EMC Office")]
        public string CarEmcTiming { get; set; }

        [Display(Name = "TurnOver N-1 With EMC")]
        public string EmcTurnOverN1 { get; set; }

        [Display(Name = "TurnOver N-2 With EMC")]
        public string EmcTurnOverN2 { get; set; }

        [Display(Name = "Have own quota")]
        public string HaveOwnQuota { get; set; }

        /*------------------- Primary Info -------------------*/

        [Display(Name = "Main Market")]
        public string MainMarkets { get; set; }

        [Display(Name = "Main Clients")]
        public string MainClients { get; set; }

        [Display(Name = "French Clients")]
        public string FrenchClients { get; set; }

        [Display(Name = "Knowledge of Standards(France)")]
        public string KnowledgeOfStandard { get; set; }

        [Display(Name = "Product Design Dept")]
        public string ProductDesignDept { get; set; }

        [Display(Name = "R & D")]
        public string RnD { get; set; }

        [Display(Name = "# of Own Factories")]
        public string NoOfOwnFactories { get; set; }

        [Display(Name = "Show Room (city name)")]
        public string ShowRoom { get; set; }

        [Display(Name = "Packaging Design Dept")]
        public string PackagingDesignDept { get; set; }

        [Display(Name = "# of Shared Factories")]
        public string NoOfSharedFactories { get; set; }

        [Display(Name = "Size of Show Room")]
        public string SizeOfShowRoom { get; set; }
    }
}
