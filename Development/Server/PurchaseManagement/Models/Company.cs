﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Company
    {
        /*------------------- Primary Info -------------------*/
        public int Id { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        public string Type { get; set; }

        public string Name { get; set; }

        public string Status { get; set; }

        [Display(Name = "SO")]
        public string So { get; set; }

        public string Factory { get; set; }

        public string Code { get; set; }

        [Display(Name = "WF Status")]
        public string WfStatus { get; set; }

        public string Team { get; set; }

        public string Zip { get; set; }

        /*------------------- Contact Info -------------------*/

        public string Address { get; set; }

        [Display(Name = "Province / State")]
        public string ProvinceOrState { get; set; }

        [Display(Name = "Phone")]
        public string PhoneNo { get; set; }

        public string Website { get; set; }

        [Display(Name = "Zip")]
        public string ZipCode { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string Fax { get; set; }

        /*------------------- Category Info -------------------*/

        [Display(Name = "Best Category")]
        public string BestCategory { get; set; }

        /*------------------- Category Info -------------------*/

        public string Currency { get; set; }

        [Display(Name = "Payment Type")]
        public string PaymentType { get; set; }

        public string Incoterm { get; set; }

        public string Moa { get; set; }

        [Display(Name = "Payment Term")]
        public string PaymentTerm { get; set; }

        public string Place { get; set; }

        /*------------------- Extra Info -------------------*/

        public string Comments { get; set; }
    }
}