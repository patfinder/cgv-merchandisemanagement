﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Factory
    {
        public int Id { get; set; }

        public int CompanyId { get; set; }

        [Display(Name = "Date Assigned")]
        public string CreatedDate { get; set; }

        [Display(Name = "Supplier / Factory")]
        public string FactoryName { get; set; }
    }
}