﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Partner : Company
    {
        [Display(Name = "Acc. Code")]
        public string AccountCode { get; set; }

        [Display(Name = "Port of Discharge")]
        public string PortOfDischarge { get; set; }

        [Display(Name = "Main Warehouse")]
        public string MainWarehouse { get; set; }

        [Display(Name = "Invoice Type")]
        public string InvoiceType { get; set; }

        public string Duration { get; set; }

        [Display(Name = "Supplier Agreement Message")]
        public string SupplierAgreementMessage { get; set; }

    }
}