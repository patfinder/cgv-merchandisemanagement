﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Logistic
    {
        public int Id { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        public string Forwarder { get; set; }

        public string Carrier { get; set; }

        [Display(Name = "Logistic Unit Type")]
        public string LogisticUnitType { get; set; }

        [Display(Name = "Transport Mode")]
        public string TransportMode { get; set; }

        [Display(Name = "Freight Payment")]
        public string FreightPayment { get; set; }

        [Display(Name = "Vessel Plane")]
        public string VesselPlane { get; set; }

        [Display(Name = "Truck/Container/Trailer #")]
        public string TruckContainerTrailerNo { get; set; }

        [Display(Name = "Loading Type")]
        public string LoadingType { get; set; }

        [Display(Name = "Voyage #")]
        public string VoyageNo { get; set; }

        [Display(Name = "Seal #")]
        public string SealNo { get; set; }

        public string Hub { get; set; }

        [Display(Name = "Port of Loading")]
        public string PortOfLoading { get; set; }

        [Display(Name = "Port of Transhipment")]
        public string PortOfTranshipment { get; set; }

        [Display(Name = "Port of Discharge")]
        public string PortOfDischarge { get; set; }

        [Display(Name = "Partner Warehouse")]
        public string PartnerWarehouse { get; set; }

        [Display(Name = "Forecast ETD")]
        public string ForecastEtd { get; set; }

        [Display(Name = "Actual ETD")]
        public string ActualEtd { get; set; }

        [Display(Name = "Forecast ETA")]
        public string ForecastEta { get; set; }

        [Display(Name = "Actual Hub Exit Date")]
        public string ActualHubExitDate { get; set; }

        public string Comments { get; set; }
    }
}