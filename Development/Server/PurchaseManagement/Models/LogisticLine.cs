﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class LogisticLine
    {
        public int Id { get; set; }

        public int LogisticId { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        public string Partner { get; set; }

        public string Supplier { get; set; }

        public string Po { get; set; }

        public string PartnerPo { get; set; }
        
        public string Pi { get; set; }

        public string Comments { get; set; }

        public string Ref { get; set; }

        public string Size { get; set; }

        public string Color { get; set; }

        [Display(Name = "Sup. Ref.")]
        public string SupRef { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        [Display(Name = "Rev. Qty")]
        public string RevQty { get; set; }

        [Display(Name = "Carton Qty")]
        public string CartonQty { get; set; }

        [Display(Name = "Forecast Volumn")]
        public string ForecastVolumn { get; set; }

        [Display(Name = "Validated Volumn")]
        public string ValidatedVolumn { get; set; }

        [Display(Name = "Forecast Gross Wt")]
        public string ForecastGrossWt { get; set; }

        [Display(Name = "Validated Gross Wt")]
        public string ValidatedGrossWt { get; set; }

        [Display(Name = "Forecast Net Wt")]
        public string ForecastNetWt { get; set; }

        [Display(Name = "Validated Net Wt")]
        public string ValidatedNetWt { get; set; }
        
        [Display(Name = "Bl #")]
        public string BlNo { get; set; }
    }
}