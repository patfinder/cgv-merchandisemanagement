﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Product
    {
        public int Id { get; set; }
        
        public string CreatedDate { get; set; }

        public string Sector { get; set; }

        public string Family { get; set; }

        [Display(Name = "Sub-Family")]
        public string SubFamily { get; set; }

        public string Category { get; set; }

        [Display(Name = "Sub-Category")]
        public string SubCategory { get; set; }

        [Display(Name = "Ref.")]
        public string Ref { get; set; }

        public string Supplier { get; set; }

        [Display(Name = "EAN/UPC")]
        public string EanUpc { get; set; }

        public string Status { get; set; }

        public string Name { get; set; }

        [Display(Name = "Supplier Ref.")]
        public string SupplierRef { get; set; }

        [Display(Name = "Valid From")]
        public string ValidFrom { get; set; }

        [Display(Name = "WF Status")]
        public string WfStatus { get; set; }

        [Display(Name = "Sourcing Type")]
        public string SourcingType { get; set; }

        [Display(Name = "SO")]
        public string So { get; set; }

        [Display(Name = "IB")]
        public string Ib { get; set; }

        public string Team { get; set; }

        public string Image { get; set; }

        [Display(Name = "Short Description")]
        public string ShortDescription { get; set; }

        public string Description { get; set; }

        [Display(Name = "Brand Position")]
        public string BrandPosition { get; set; }

        [Display(Name = "First Season")]
        public string FirstSeason { get; set; }

        public string Composition { get; set; }

        [Display(Name = "Prod. Peak Season")]
        public string ProdPeakSeason { get; set; }

        [Display(Name = "Prod. Low Season")]
        public string ProdLowSeason { get; set; }

        [Display(Name = "Avail. Qty")]
        public string AvailableQuantity { get; set; }

        [Display(Name = "Net Weight")]
        public string NetWeight { get; set; }

        [Display(Name = "Seasonal Comments")]
        public string SeasonalComments { get; set; }

        public string Promotional { get; set; }

        public string Set { get; set; }

        [Display(Name = "Size (LxWxH)")]
        public string Size { get; set; }

        [Display(Name = "Gross Weight")]
        public string GrossWeight { get; set; }

        [Display(Name = "Net Content")]
        public string NetContent { get; set; }

        [Display(Name = "Country of Origin")]
        public string CountryOfOrigin { get; set; }

        [Display(Name = "MOQ Manufacturing")]
        public string MoqManufacturing { get; set; }

        public string Incoterm { get; set; }

        public string Price { get; set; }

        public string Place { get; set; }

        [Display(Name = "Default Logistics")]
        public string DefaultLogistics { get; set; }

        [Display(Name = "NB SKU/MC")]
        public string NbSkuMc { get; set; }

        [Display(Name = "Sizeo of MC (LxWxH)")]
        public string SizeOfMc { get; set; }

        [Display(Name = "SKU in 20' Container")]
        public string SkuIn20InContainer { get; set; }

        [Display(Name = "Gross Wt. of MC")]
        public string GrossWtOfMc { get; set; }

        public string Comments { get; set; }
    }
}