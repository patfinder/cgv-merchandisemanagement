﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace PurchaseManagement.Models
{
    public class Validation
    {
        public int Id { get; set; }

        public int LogisticId { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }
    }
}