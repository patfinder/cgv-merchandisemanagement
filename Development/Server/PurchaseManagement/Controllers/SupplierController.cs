﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseManagement.Models;
using PurchaseManagement.Services;
using PurchaseManagement.ViewModels;

namespace PurchaseManagement.Controllers
{
    public class SupplierController : Controller
    {
        private MerchandiseContext db = new MerchandiseContext();

        public ViewResult Index()
        {
            return View(db.Suppliers);
        }

        [HttpPost]
        public ViewResult Index(SearchSupplierViewModels model)
        {
            IEnumerable<Supplier> suppliers = db.Suppliers;
            if (!string.IsNullOrEmpty(model.Name))
            {
                model.Name = model.Name.ToLower();
                suppliers = suppliers.Where(supplier => supplier.Name.ToLower().Contains(model.Name));
            }

            return View(suppliers);
        }

        // GET: Supplier
        public ActionResult Create()
        {
            return View(new CreateSupplierViewModels());
        }

        [HttpPost]
        public ActionResult Create(CreateSupplierViewModels model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Suppliers.Add(model);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(model);
        }

        public ActionResult Main(int id)
        {
            var supplier = db.Suppliers.FirstOrDefault(s => s.Id == id) ?? new Supplier();
            return View(supplier);
        }

        public ActionResult Details(int id)
        {
            var companyDetails = db.CompanyDetails.FirstOrDefault(s => s.Id == id) ?? new CompanyDetails();
            return View(companyDetails);
        }

        public ActionResult Contacts(int id)
        {
            var company = db.Suppliers.FirstOrDefault(s => s.Id == id);
            var contacts = db.Contacts.Where(s => s.CompanyId == id);
            return View(new CompanyContactsViewModels
            {
                Company = company,
                Contacts = contacts
            });
        }

        public ActionResult Factories(int id)
        {
            var company = db.Suppliers.FirstOrDefault(s => s.Id == id);
            var factories = db.Factories.Where(s => s.CompanyId == id);
            return View(new CompanyFactoriesViewModels
            {
                Company = company,
                Factories = factories
            });
        }
    }
}