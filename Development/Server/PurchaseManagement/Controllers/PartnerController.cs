﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseManagement.Models;
using PurchaseManagement.Services;
using PurchaseManagement.ViewModels;

namespace PurchaseManagement.Controllers
{
    public class PartnerController : Controller
    {
        private MerchandiseContext db = new MerchandiseContext();

        // GET: Partner
        public ActionResult Index()
        {
            return View(db.Partners);
        }

        [HttpPost]
        public ViewResult Index(SearchPartnerViewModel model)
        {
            IEnumerable<Partner> partners = db.Partners;
            if (!string.IsNullOrEmpty(model.Name))
            {
                model.Name = model.Name.ToLower();
                partners = partners.Where(partner => partner.Name.ToLower().Contains(model.Name));
            }

            return View(partners);
        }

        public ActionResult Create()
        {
            return View(new CreatePartnerViewModel());
        }

        [HttpPost]
        public ActionResult Create(CreatePartnerViewModel model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Partners.Add(model);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(model);
        }

        public ActionResult Main(int id)
        {
            var partner = db.Partners.FirstOrDefault(s => s.Id == id) ?? new Partner();
            return View(partner);
        }

        public ActionResult Addresses(int id)
        {
            return RedirectToAction("Index", "Address", new {id, type = "partner"});
        }
    }
}