﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseManagement.Models;
using PurchaseManagement.Services;
using PurchaseManagement.ViewModels;

namespace PurchaseManagement.Controllers
{
    public class LogisticController : Controller
    {
        private MerchandiseContext db = new MerchandiseContext();

        // GET: Address
        public ActionResult Index(SearchLogisticViewModel model)
        {
            //if (model == null)
            //    model = new SearchProductViewModel();

            //var products = db.Products as IQueryable<Product>;

            //if (!string.IsNullOrEmpty(model.Ref))
            //{
            //    model.Ref = model.Ref.ToLower();
            //    products = products.Where(p => p.Ref.ToLower().Contains(model.Ref));
            //}

            //if (!string.IsNullOrEmpty(model.Supplier))
            //{
            //    model.Supplier = model.Supplier.ToLower();
            //    products = products.Where(p => p.Supplier.ToLower().Contains(model.Supplier));
            //}

            //if (!string.IsNullOrEmpty(model.Keyword))
            //{
            //    model.Keyword = model.Keyword.ToLower();
            //    products = products.Where(p => p.ShortDescription.ToLower().Contains(model.Keyword) || p.Description.ToLower().Contains(model.Keyword));
            //}

            return View(db.Logistics);
        }

        public ActionResult Create()
        {
            return View(new Logistic());
        }

        [HttpPost]
        public ActionResult Create(Logistic model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Logistics.Add(model);
                    db.SaveChanges();
                    //return RedirectToAction("Index");
                    return View(model);
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(model);
        }

        public ActionResult Main(int id)
        {
            var logistic = db.Logistics.FirstOrDefault(p => p.Id == id) ?? new Logistic();
            return View(logistic);
        }

        public ActionResult AssignLines(int id)
        {
            var logistic = db.Logistics.FirstOrDefault(p => p.Id == id) ?? new Logistic();
            var availableLines = new[]
            {
                new LogisticLine(), new LogisticLine(), new LogisticLine(), new LogisticLine(), new LogisticLine(),
                new LogisticLine()
            };
            var lines = db.LogisticLines.Where(l => l.LogisticId == id).ToList();
            return
                View(new AssignLinesLogisticViewModel
                {
                    Logistic = logistic,
                    AvailableLines = availableLines,
                    Lines = lines
                });
        }

        public ActionResult Validation(int id)
        {
            var logistic = db.Logistics.FirstOrDefault(p => p.Id == id) ?? new Logistic();
            var availableLines = new[]
            {
                new LogisticLine(), new LogisticLine(), new LogisticLine(), new LogisticLine(), new LogisticLine(),
                new LogisticLine()
            };
            var lines = db.LogisticLines.Where(l => l.LogisticId == id).ToList();
            return
                View(new AssignLinesLogisticViewModel
                {
                    Logistic = logistic,
                    Lines = lines
                });
        }
    }
}