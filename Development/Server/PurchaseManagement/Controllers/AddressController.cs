﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseManagement.Models;
using PurchaseManagement.Services;
using PurchaseManagement.ViewModels;

namespace PurchaseManagement.Controllers
{
    public class AddressController : Controller
    {
        private MerchandiseContext db = new MerchandiseContext();

        // GET: Address
        public ActionResult Index(int id, string type = "Supplier")
        {
            type = type?.ToLower();

            var companyType = type == "supplier" ? CompanyType.Supplier : CompanyType.Partner;

            var company = companyType == CompanyType.Supplier
                ? db.Suppliers.FirstOrDefault(s => s.Id == id) as Company
                : db.Partners.FirstOrDefault(s => s.Id == id);

            var addresses = db.Addresses.Where(s => s.CompanyId == id && s.CompanyType == companyType);
            return View(new CompanyAddressesViewModels
            {
                Company = company,
                Addresses = addresses
            });
        }
    }
}