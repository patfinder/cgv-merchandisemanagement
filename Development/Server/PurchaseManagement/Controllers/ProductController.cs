﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using PurchaseManagement.Models;
using PurchaseManagement.Services;
using PurchaseManagement.ViewModels;

namespace PurchaseManagement.Controllers
{
    public class ProductController : Controller
    {
        private MerchandiseContext db = new MerchandiseContext();

        // GET: Address
        public ActionResult Index(SearchProductViewModel model)
        {
            if(model == null)
                model = new SearchProductViewModel();

            var products = db.Products as IQueryable<Product>;

            if (!string.IsNullOrEmpty(model.Ref))
            {
                model.Ref = model.Ref.ToLower();
                products = products.Where(p => p.Ref.ToLower().Contains(model.Ref));
            }

            if (!string.IsNullOrEmpty(model.Supplier))
            {
                model.Supplier = model.Supplier.ToLower();
                products = products.Where(p => p.Supplier.ToLower().Contains(model.Supplier));
            }

            if (!string.IsNullOrEmpty(model.Keyword))
            {
                model.Keyword = model.Keyword.ToLower();
                products = products.Where(p => p.ShortDescription.ToLower().Contains(model.Keyword) || p.Description.ToLower().Contains(model.Keyword));
            }

            return View(products);
        }

        public ActionResult Create()
        {
            return View(new Product());
        }

        [HttpPost]
        public ActionResult Create(Product model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Products.Add(model);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
            }
            catch (RetryLimitExceededException /* dex */)
            {
                //Log the error (uncomment dex variable name and add a line here to write a log.
                ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists see your system administrator.");
            }
            return View(model);
        }

        public ActionResult Main(int id)
        {
            var product = db.Products.FirstOrDefault(p => p.Id == id) ?? new Product();
            return View(product);
        }
    }
}